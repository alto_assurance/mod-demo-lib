import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { Response } from '../models/response';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }

  newUser(user:User):Promise<Response>{
    return this.http.post<Response>('http://localhost:8010/base/api/v1/user',user).toPromise()
  }
  listUsers(param:any):Promise<Array<User>>{
    return this.http.get<Array<User>>('http://localhost:8010/base/api/v1/user/list?param=xxxx').toPromise()
  }
}

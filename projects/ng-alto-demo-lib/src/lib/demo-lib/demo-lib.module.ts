import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'; 
import { HttpClientModule } from '@angular/common/http';

import { DemoLibRoutingModule } from './demo-lib-routing.module';
import { MainComponent } from './components/main/main.component';
import { UserComponent } from './components/user/user.component';

@NgModule({
  declarations: [MainComponent, UserComponent],
  imports: [
    CommonModule,
    DemoLibRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  exports:[MainComponent,UserComponent]
})
export class DemoLibModule { }

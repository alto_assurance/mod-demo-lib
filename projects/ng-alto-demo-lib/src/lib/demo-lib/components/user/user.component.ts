import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
import { Response } from '../../models/response';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'demo-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private userService:UserService) { }
  showLoading:boolean=false;
  message:string = "loading..."
  // two-way binding
  user:User = {
    name:'Adan',
    lastname:'Escobar',
    email:'aescobar@grupoalto.com',
    username:'aescobar',
    password:'12345'
  }

  ngOnInit() {
  }

  newUser(){
    console.log(this.user)
    this.showLoading = true;
    this.message = 'Creating User';

    this.userService.newUser(this.user).then((resp:Response) => {
        console.log(resp)
        this.showLoading = false;
        alert('Usuario creado !!');
    }).catch((err: HttpErrorResponse) => {
      this.showLoading = false;

      alert(err.error.message)
    })
  }
  listUsers(){
    this.message = 'Cargando usuarios';

    this.userService.listUsers('').then((resp:User[]) => {
        console.log(resp)
    }).catch((err: HttpErrorResponse) => {
      this.showLoading = false;
      alert(err.error.message)
    })
  }

}
